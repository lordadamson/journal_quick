.pragma library
.import QtQuick 2.10 as QtWiki

var component;
var sprite;

function createSpriteObjects(myCanvas, x, y)
{
	component = Qt.createComponent("JText.qml");

	if (component.status === QtWiki.Component.Ready)
	{
		finishCreation(myCanvas, x, y);
	}
	else if(component.status === QtWiki.Component.Error)
	{
		console.log(component.errorString())
	}
	else
	{
		component.statusChanged.connect(
			function()
			{
				finishCreation(myCanvas, x, y)
			});
	}
}

function finishCreation(myCanvas, x, y)
{
	if (component.status === QtWiki.Component.Ready)
	{
		sprite = component.createObject(myCanvas, {"x": x, "y": y});

		if (sprite === null)
		{
			console.log("Error creating object");
		}
	}
	else if (component.status === QtWiki.Component.Error)
	{
		console.log("Error loading component:", component.errorString());
	}
}
