import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Window
{
	visible: true
	width: 640
	height: 480
	title: qsTr("Hello World")
	color: "#fff"

	GridLayout
	{
		columns: 1
		anchors.fill: parent

		RowLayout
		{
			Button
			{
				id: brush_btn
				text: "Brush"
				onClicked: jcanvas.brush = true
			}

			Button
			{
				id: text_btn
				text: "Text"
				onClicked: jcanvas.brush = false
			}
		}

		RowLayout
		{
			JCanvas
			{
				id: jcanvas
			}
		}
	}
}
