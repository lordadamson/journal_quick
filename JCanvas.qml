import QtQuick 2.0
import "create_text.js" as Create_text

Canvas
{
	property int xpos
	property int ypos
	property int last_xpos
	property int last_ypos

	property bool brush : true

	id: myCanvas
	anchors.fill: parent

	onPaint:
	{
		var ctx = getContext('2d')
		ctx.beginPath()
		ctx.moveTo(last_xpos, last_ypos)
		ctx.lineTo(xpos, ypos)
		ctx.stroke()
		last_xpos = xpos
		last_ypos = ypos
		ctx.closePath()
	}

	MouseArea
	{
		anchors.fill: parent
		onPressed:
		{
			myCanvas.xpos = mouseX
			myCanvas.ypos = mouseY
			myCanvas.last_xpos = mouseX
			myCanvas.last_ypos = mouseY

			if(brush)
			{
				myCanvas.requestPaint()
			}
			else
			{
				Create_text.createSpriteObjects(this, xpos, ypos)
			}
		}
		onMouseXChanged:
		{
			if(brush)
			{
				myCanvas.xpos = mouseX
				myCanvas.ypos = mouseY
				myCanvas.requestPaint()
			}
		}
		onMouseYChanged:
		{
			if(brush)
			{
				myCanvas.xpos = mouseX
				myCanvas.ypos = mouseY
				myCanvas.requestPaint()
			}
		}
	}
}
